/////////////////////////////////////////////////////////////////////////
// functions.m
/////////////////////////////////////////////////////////////////////////
// Authors: Alexander Kasprzyk and Gary McConnell
/////////////////////////////////////////////////////////////////////////
/*
The range of input data we're ultimately interesting in checking:
d = 6
p = 5, 7, 11, 13
q = p, p^2
*/

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// The recursive part of all_bases below.
function all_bases_recursive(S,basis,d)
    bases:={PowerSet(Universe(basis))|};
    while #S ge d - #basis do
        if #basis eq d - 1 then
            for u in S do
                Include(~bases,Include(basis,u));
            end for;
            S:=[];
        elif #S eq d - #basis then
            u:=S[1];
            Remove(~S,1);
            ip:=SequenceToSet(InnerProduct(u,S));
            if #ip eq 1 and Representative(ip) eq -1 then
                Include(~basis,u);
            end if;
        else
            u:=S[1];
            Remove(~S,1);
            ip:=InnerProduct(u,S);
            newS:=[Universe(S) | S[i] : i in [1..#S] | ip[i] eq -1];
            bases join:= $$(newS,Include(basis,u),d);
        end if;
    end while;
    return bases;
end function;

// Returns the set of all basis B that can be constructed from the sequence S
// s.t. v in B. Assumes that S is sorted in increasing order.
function all_bases(v,S)
    d:=Rank(Parent(v)) + 1;
    ip:=InnerProduct(v,S);
    S:=[Universe(S) | S[i] : i in [1..#S] | ip[i] eq -1];
    return all_bases_recursive(S,{v},d);
end function;

// Returns the subsequence of S of elements v such that v is mub to B.
function mub_subsequence(B,S)
    // Extract the data we need
    K:=CoefficientRing(Universe(B));
    bool,p,n:=IsQuadraticExtension(K);
    assert bool;
    d:=#B;
    // Build the matrices
    M1:=Transpose(Matrix(SetToSequence(B)));
    M2:=Transpose(Matrix(S));
    M1conj:=Transpose(Matrix(d-1,d,FrobeniusConjugate(Eltseq(M1))));
    // Calculate product matrix
    MM:=M1conj * M2 + Matrix(d,#S,[K | 1 : i in [1..d * #S]]);
    // Build the set of powers of X^(q-1)
    q:=p^n;
    Y:=K.1^(q-1);
    pows:={K | Y^k : k in [1..q + 1]};
    // Extract the mub elements from S
    return [Universe(S) | S[i] : i in [1..#S] |
                                &and[MM[j,i] in pows : j in [1..d]]];
end function;

// Returns a largest set of mubs containing BB that can be created using only
// elements in S. Assumes that S is sorted in increasing order, and that all
// elements in S are mub to the elements in BB.
function extend_mubs(S,BB)
    largest_mub:=BB;
    while #S ne 0 do
        // Pop the first (i.e. smallest) element from S
        v:=S[1];
        Remove(~S,1);
        // Attempt to extend v to a basis in all possible ways
        for B in all_bases(v,S) do
            // Prune S down to those elements mub to B and recurse
            newS:=mub_subsequence(B,S);
            new_largest_mub:=$$(newS,Include(BB,B));
            if #new_largest_mub gt #largest_mub then
                largest_mub:=new_largest_mub;
            end if;
        end for;
    end while;
    return largest_mub;
end function;

// Lifts the basis B to the vector-space VS using element sigma.
function lift_basis(B,VS,sigma)
    d:=Rank(VS);
    assert #B eq d and Rank(Universe(B)) eq d - 1;
    return {VS | [sigma] cat [sigma * v[i] : i in [1..d - 1]] : v in B};
end function;

/////////////////////////////////////////////////////////////////////////
// Main functions
/////////////////////////////////////////////////////////////////////////

// Returns a mub of maximum length in the d-dimensional vector-space
// over GF(q^2). This only starts to build bases in the range [start..finish],
// and is used by the HPC to partition the search space.
function maximum_mubs_in_range(d,q,start,finish)
    // Sanity check
    error if not IsIntegral(d) or d le 0,
        "Argument 1 must be a positive integer";
    d:=Integers() ! d;
    error if not IsIntegral(q), "Argument 2 must be a prime power";
    q:=Integers() ! q;
    error if GCD(d,q) ne 1, "Arguments 1 and 2 must be coprime";
    error if  not IsPrimePower(q), "Argument 2 must be a prime power";
    error if not IsIntegral(start) or start le 0,
        "Argument 3 must be a positive integer";
    start:=Integers() ! start;
    error if not IsIntegral(finish) or finish lt start,
        Sprintf("Argument 4 must be an integer at least equal to %o",start);
    finish:=Integers() ! finish;
    // Create the finite field K = GF(q^2)
    K:=GF(q^2);
    // Generate the sequence S of all candidate (d-1)-dimensional invertible
    // elements of norm 1/n
    G,phi:=MultiplicativeGroup(K);
    U:=sub<G | (q-1)*G.1>;
    error if finish gt (#U)^(d-1),
        Sprintf("Argument 4 must be at most %o",(#U)^(d-1));
    CU:=CartesianPower(U,d-1);
    S:=[VectorSpace(K,d-1) | [phi(c) : c in u] : u in CU];
    // Sort S (that S is sorted is an important assumption in what follows) and
    // trim off the entries up to the starting entry
    Sort(~S);
    S:=S[start..#S];
    // Create a longest mub
    BB:={PowerSet(Universe(S))|};
    for i in [1..finish - start + 1] do
        // Pop the first (i.e. smallest) element from S
        v:=S[1];
        Remove(~S,1);
        // Attempt to extend v to a basis in all possible ways
        for B in all_bases(v,S) do
            // Prune S down to those elements mub to B and extend
            newS:=mub_subsequence(B,S);
            new_BB:=extend_mubs(newS,{B});
            if #new_BB gt #BB then
                BB:=new_BB;
            end if;
        end for;
    end for;
    // Build the final d-dimensional vector-space over K
    VS:=VectorSpace(K,d);
    // Pick an element sigma that will allow us to lift our bases to VS
    dinv:=K ! (1/d);
    sigma:=Min({phi(s) : s in G | phi(s) * FrobeniusConjugate(phi(s)) eq dinv});
    // Finally, lift the bases in BB using sigma and include the computational
    // basis
    BBlift:={PowerSet(VS) | lift_basis(B,VS,sigma) : B in BB};
    Include(~BBlift,ComputationalBasis(VS,d));
    assert AreOrthononormalMUBs(BBlift);
    return BBlift;
end function;

// Returns a mub of maximum length in the d-dimensional vector-space
// over GF(q^2).
function maximum_mubs(d,q)
    // Sanity check
    error if not IsIntegral(d) or d le 0,
        "Argument 1 must be a positive integer";
    d:=Integers() ! d;
    error if not IsIntegral(q), "Argument 2 must be a prime power";
    q:=Integers() ! q;
    error if  not IsPrimePower(q), "Argument 2 must be a prime power";
    error if GCD(d,q) ne 1, "Arguments must be coprime";
    // Create the finite field K = GF(q^2)
    K:=GF(q^2);
    // Generate the sequence S of all candidate (d-1)-dimensional invertible
    // elements of norm 1/n
    G,phi:=MultiplicativeGroup(K);
    U:=sub<G | (q-1)*G.1>;
    CU:=CartesianPower(U,d-1);
    S:=[VectorSpace(K,d-1) | [phi(c) : c in u] : u in CU];
    // Sort S (that S is sorted is an important assumption in what follows)
    Sort(~S);
    // Create a longest mub
    BB:=extend_mubs(S,{PowerSet(Universe(S))|});
    // Build the final d-dimensional vector-space over K
    VS:=VectorSpace(K,d);
    // Pick an element sigma that will allow us to lift our bases to VS
    dinv:=K ! (1/d);
    sigma:=Min({phi(s) : s in G | phi(s) * FrobeniusConjugate(phi(s)) eq dinv});
    // Finally, lift the bases in BB using sigma and include the computational
    // basis
    BBlift:={PowerSet(VS) | lift_basis(B,VS,sigma) : B in BB};
    Include(~BBlift,ComputationalBasis(VS,d));
    assert AreOrthononormalMUBs(BBlift);
    return BBlift;
end function;
