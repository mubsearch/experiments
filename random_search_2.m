// Characteristic of base field. Need p > 2 else d^-1 = 1 and so ON and
// MUBness coincide.
p:=7;
// p^n = order of base field; we work over the quadratic extension GF(p^2n) of
// GF(p^n). Need n >= 1.
n:=1;
// The dimension of the vector (sub-)spaces GF(p^2n)^d of the ambient space
// spanned by the (mini-)MUBs. Need d > 1 else trivial. A (mini-)MUB consists
// of d vectors.
d:=6;
// Max number of bases sought including complementary basis.
M:=4;

// Create the finite field
K:=GF((p^n)^2);
// Generate the set S of all candidate (d-1)-dimensional invertible elements of
// norm 1/n
G,phi:=MultiplicativeGroup(K);
U:=sub<G | (p^n - 1) * G.1>;
CU:=CartesianPower(U,d - 1);
VVS:=VectorSpace(K,d - 1);

// Returns the subset of S of elements v such that v is mub to B.
function mubilise(B,S)
    // Extract the data we need
    K:=CoefficientRing(Universe(B));
    bool,p,n:=IsQuadraticExtension(K);
    assert bool;
    d:=#B;
    if d eq 0 then return S; end if;
    // Build the matrices
    S:=SetToSequence(S);
    M1:=Transpose(Matrix(SetToSequence(B)));
    M2:=Transpose(Matrix(S));
    M1conj:=Transpose(Matrix(d-1,d,FrobeniusConjugate(Eltseq(M1))));
    // Calculate product matrix
    MM:=M1conj * M2 + Matrix(d,#S,[K | 1 : i in [1..d * #S]]);
    // Build the set of powers of X^(q-1)
    q:=p^n;
    Y:=K.1^(q-1);
    pows:={K | Y^k : k in [1..q + 1]};
    // Extract the mub elements from S
    return {Universe(S) | S[i] : i in [1..#S] |
                                &and[MM[j,i] in pows : j in [1..d]]};
end function;

// Attempts to generate an ON basis out of vectors in S, following order
// specified in permutation perm. Returns true followed by the basis on success,
// false otherwise.
function gen_ONB(S,perm,d)
    basis:=[Universe(S)|];
    for v in PermuteSequence(SetToSequence(S),perm) do
        if #basis eq 0 then
            Append(~basis,v);
        else
            ip:=SequenceToSet(InnerProduct(v,basis));
            if #ip eq 1 and Representative(ip) eq -1 then
                Append(~basis,v);
                if #basis eq d then
                    return true,SequenceToSet(basis);
                end if;
            end if;
        end if;
    end for;
    return false,_;
end function;

// Create the data structures we need
flag:=0;
longest_so_far:={PowerSet(VVS)|};
bases:={PowerSet(VVS)|};
mubiles:={VVS | [phi(c) : c in Random(CU)] : i in [1..10000]};

// Start hunting
while #bases lt M - 1 and flag lt 5 do
    #mubiles;
    if #mubiles lt d then
        success:=false;
    else
        perm:=Random(Sym(#mubiles));
        success,base:=gen_ONB(mubiles,perm,d);
        if success then
            Include(~bases,base);
            mubiles:=mubilise(base,mubiles);
        end if;
    end if;
    if #bases gt #longest_so_far then
        longest_so_far:=bases;
        printf "longest so far: %o + 1\n",#bases;
    end if;
    if not success then
        if #bases eq 0 then
            flag +:= 1;
        end if;
        printf "end of this line: %o + 1 bases\n",#bases;
        bases:={PowerSet(VVS)|};
        mubiles:={VVS | [phi(c) : c in Random(CU)] : i in [1..10000]};
    end if;
end while;

// Build the final d-dimensional vector-space over K
VS:=VectorSpace(K,d);
// Pick an element sigma that will allow us to lift our bases to VS
dinv:=K ! (1 / d);
sigma:=Min({phi(s) : s in G | phi(s) * FrobeniusConjugate(phi(s)) eq dinv});

// Lifts the basis B to the vector-space VS using element sigma.
function lift_basis(B,VS,sigma)
    d:=Rank(VS);
    assert #B eq d and Rank(Universe(B)) eq d - 1;
    return {VS | [sigma] cat [sigma * v[i] : i in [1..d - 1]] : v in B};
end function;

BBlift:={PowerSet(VS) | lift_basis(B,VS,sigma) : B in longest_so_far};
Include(~BBlift,ComputationalBasis(VS,d));
assert AreOrthononormalMUBs(BBlift);
BBlift;
