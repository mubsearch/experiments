// Characteristic of base field. Need p > 2 else d^-1 = 1 and so ON and
// MUBness coincide.
p:=5;
// p^n = order of base field; we work over the quadratic extension GF(p^2n) of
// GF(p^n). Need n >= 1.
n:=1;
// The dimension of the vector (sub-)spaces GF(p^2n)^d of the ambient space
// spanned by the (mini-)MUBs. Need d > 1 else trivial. A (mini-)MUB consists
// of d vectors.
d:=6;
// N >= d is the dimension of the ambient vector space GF(p^2n)^N in which we
// work. N = d gives classical MUBs case.
N:=6;
// Max number of bases sought including complementary basis.
M:=4;

// Create the space we're working in
K<X>:=GF((p^n)^2);
VS:=VectorSpace(K,N);

// Candidate vex for MUBs wrt ON basis ctg vec
function mubilize(V,NN,d)
    return #V eq 0 select NN else {Universe(NN) | w : w in NN | IsMUB(w,V,d)};
end function;

// Attempts to generate an ON basis out of vectors in vex, following order
// specified in permutation perm. Returns true followed by the basis on success,
// false otherwise.
function gen_ONB(vex,perm,d)
    base:={Universe(vex)|};
    for mu in PermuteSequence(SetToSequence(vex),perm) do
        if IsOrthogonal(mu,base) then
            Include(~base,mu);
            if #base eq d then
                return true,base;
            end if;
        end if;
    end for;
    return false,_;
end function;

bases:={PowerSet(VS)|};
flag:=0;
longest_so_far:={PowerSet(VS)|};
bases_atomized:={VS|};
NN:=Norm1Vectors(VS,d);

while #NN ge d and #bases lt M - 1 and flag lt 5 do
    NN:=NN diff bases_atomized;
    mubiles:=mubilize(bases_atomized,NN,d);
    if #mubiles eq 0 then
        success:=false;
    else
        perm:=Random(Sym(#mubiles));
        success,base:=gen_ONB(mubiles,perm,d);
    end if;
    if not success then
        if #bases gt #longest_so_far then
            longest_so_far:=bases;
        end if;
        if #bases eq 0 then
            flag +:= 1;
        end if;
        printf "end of this line %o bases incl comp basis\n",#bases + 1;
        bases:={PowerSet(VS)|};
        bases_atomized:={VS|};
    else
        Include(~bases,base);
        if #bases gt #longest_so_far then
            longest_so_far:=bases;
            printf "bases so far = %o\n", bases;
            print #bases;
        end if;
        bases_atomized join:= base;
    end if;
end while;

Include(~longest_so_far,ComputationalBasis(VS,d));
assert AreOrthononormalMUBs(longest_so_far);

printf "largest set of MUBs found has %o elements including the computational basis:\n%o\n",#longest_so_far,longest_so_far;
