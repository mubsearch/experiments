p:=23;
assert (p mod 24) eq 23;
// This only works for d = 6
d:=6;
assert d eq 6;

// Create the finite field
K:=GF(p^2);

// Pick a generator g for the multiplicative subgroup
G,phi:=MultiplicativeGroup(K);
g:=G.1;
// We need the d-th root of unity
w:=Eltseq(RootOfUnity(d,K) @@ phi)[1];
assert phi(d * w * g) eq K ! 1;
// We need the 2d-th root of unity
w2:=Eltseq(RootOfUnity(2 * d,K) @@ phi)[1];
assert phi(2 * d * w * g) eq K ! 1;
// We also need the root of d
rt:=Eltseq(Sqrt(K ! d) @@ phi)[1];
assert phi(2 * rt * g) eq d;

// The d-dimensional vector-space over K that holds our mubs
VS:=VectorSpace(K,d);

// Create the computational basis, MUB_0
MUB0:=SequenceToSet(Basis(VS));
assert IsOrthonormal(MUB0);
// Create MUB_1
Fpow:=Matrix(d,d,[Integers() | w * i * j :
                               j in [0..d - 1], i in [0..d - 1]]);
MUB1:={VS | [K | phi((Fpow[i,j] - rt) * g) : j in [1..d]] : i in [1..d]};
assert IsOrthonormal(MUB1);
assert IsMUB(MUB0,MUB1);
// Create MUB_2
shift:=Append([2..d],1);
gen:=[K | phi((w2 * ((j^2 - 1) mod (2 * d)) - rt) * g) : j in [1..d]];
MUB2:={VS | gen};
for i in [2..6] do
    gen:=[K | gen[j] : j in shift];
    Include(~MUB2,gen);
end for;
assert IsOrthonormal(MUB2);
assert IsMUB(MUB0,MUB2);
assert IsMUB(MUB1,MUB2);