d:=6;
p:=13;
q:=p^1;

F:=GF(q^2);

G,phi:=MultiplicativeGroup(F);
u:=(q - 1) * G.1;
U:=sub<G | u>;

dinv:=F ! (1 / d);
delta:=Representative({F | s : s in F | s * FrobeniusConjugate(s) eq dinv});

UU:=CartesianPower(U,d-1);

UtoZ:=AssociativeArray(U);
for i in [0..Order(U) - 1] do
    UtoZ[i * u]:=i;
end for;

function psi(m)
    return &+[F | phi(u * UtoZ[i]) : i in m];
end function;

FF:=CartesianPower(F,d-1);

N0:={FF | <phi(i) : i in m> : m in UU | psi(m) + 1 eq 0};

deltabarinv:=1 / FrobeniusConjugate(delta);
dS:={F | deltabarinv * phi(u) : u in U};
NS:={FF | <phi(i) : i in m> : m in UU | psi(m) + 1 in dS};

function extend_to_Ms(M,myN0)
    if #M eq d - 1 then
        return {M};
    end if;
    Ms:={PowerSet(Universe(M))|};
    newN0:=myN0;
    for m in myN0 do
        Exclude(~newN0,m);
        if &and[(FF ! <mm[i] / m[i] : i in [1..d - 1]>) in N0 : mm in M] then
            newM:=Include(M,m);
            Ms join:= $$(newM,newN0);
        end if;
    end for;
    return Ms;
end function;

v:=FF ! <phi(i) : i in Random(UU)>;
Ms:=extend_to_Ms({Universe(N0)|},N0);